#!/bin/bash
# License: GPLv3
# Author: mikhailnov
# common functions and variables for scripts 

tmp_dir="/tmp/m2m_$(date +%s)"
lang="ru_RU"
input_dir="input-data"
in_old_lo="${input_dir}/libreoffice-old"
in_new_aot="${input_dir}/ie-yo-aot-new"

old_dic="${in_old_lo}/${lang}.dic"
old_aff="${in_old_lo}/${lang}.aff"
new_dic="${in_new_aot}/${lang}.dic"
new_aff="${in_new_aot}/${lang}.dic"
tmp_old_dic="${tmp_dir}/tmp_old_dic.txt"
tmp_new_dic="${tmp_dir}/tmp_new_dic.txt"
tmp_merged_dic="${tmp_dir}/tmp_merged_dic.txt"
tmp_merged_dic_sorted="${tmp_dir}/tmp_merged_dic_sorted.txt"

echo_err(){
	echo "$@" >&2
}

nol(){
	# count number of lines in file
	wc -l "$1" | awk '{print $1}'
}

check_locale(){
	if ! locale -a | grep -iqE "$@"; then
		echo_err "В системе не обнаружена поддержка локали $@ . Расскоментируйте ее в /etc/locale.gen и сделайте sudo locale-gen."
		return 1	
	fi
}

rm -fvr "${tmp_dir}/*"
mkdir -p "${tmp_dir}"

