#!/bin/bash
# License: GPLv3
# Author: mikhailnov
# compare 2 dictionaries

source common-funcs.sh || exit 1

# посчитаем текущее количество строк в файлах словарей
# nol - number of lines
nol_old_dic="$(nol "$old_dic")"
nol_new_dic="$(nol "$new_dic")"

rm -fv "$tmp_merged_dic" "$tmp_merged_dic_sorted"

cat "$old_dic" | iconv -f KOI8-R -t UTF-8 | awk -F "/" '{print $1}' | sort --unique >> "$tmp_old_dic"
cat "$new_dic" | iconv -f KOI8-R -t UTF-8 | awk -F "/" '{print $1}' | sort --unique >> "$tmp_new_dic"

cat "$tmp_old_dic" "$tmp_new_dic" >> "$tmp_merged_dic"

nol_common="$(comm -1 -2 "$tmp_old_dic" "$tmp_new_dic" | wc -l)"
#nol_tmp_merged_dic="$(nol "$tmp_merged_dic")"

cat "$tmp_merged_dic" | sort --ignore-case --unique --output="$tmp_merged_dic_sorted"
nol_tmp_merged_dic_sorted="$(nol "$tmp_merged_dic_sorted")"

echo ""
echo "Старый словарь содержит ${nol_old_dic} строк"
echo "Новый словарь содержит ${nol_new_dic} строк"
echo "В них ${nol_common} одинаковых слов."
echo ""
echo "Соединенный и отсортированный словарь расположен по адресу ${tmp_merged_dic_sorted} и содержит ${nol_tmp_merged_dic_sorted} строк."
echo ""
