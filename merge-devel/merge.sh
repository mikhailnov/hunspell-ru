#!/bin/bash
# License: GPLv3
# Author: mikhailnov
# Этот скрипт должен соединить старый словарь hunspell, поддержимваемый в дереве исходников Libreoffice, с новым словарем AOT Е-Ё 0.3.9
# http://forumooo.ru/index.php/topic,106.60.html
# https://extensions.openoffice.org/en/projectrelease/russkiy-orfograficheskiy-slovar-aot-eyo-russian-spellcheck-dict-ieyo-based-works

source common-funcs.sh || exit 1

# убедимся, что ru_RU.aff (набор грамматических пподстановок) одинаковый и там, и там.
#for i in aff
#do
	#aff_sum_old="$(sha256sum "${in_old_lo}/${lang}.${i}" | awk '{print $1}')"
	#aff_sum_new="$(sha256sum "${in_new_aot}/${lang}.${i}" | awk '{print $1}')"
	#if [ "${aff_sum_old}" != "${aff_sum_new}" ]; then
		#echo_err ".aff files are not the same, cannot continue merging .dic files!"
		#exit 1
	#fi
#done



