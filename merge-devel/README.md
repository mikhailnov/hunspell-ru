Здесь ведется работа по соединению (объединению) двух словарей, по возможности, без потерь.

* **Старый словарь** — это словарь hunspell из апстрима Libreoffice ([ссылка](https://cgit.freedesktop.org/libreoffice/dictionaries/tree/ru_RU)).
* **Новый словарь** — это словарь hunspell ie-yo-AOT.

## common-funcs.sh
bash-функции, используемые в обоих скриптах.

## compare.sh
Сравнивает 2 словаря.

Пример запуска и вывода:

```
$ ./compare.sh

Старый словарь содержит 146270 строк
Новый словарь содержит 379821 строк
В них 90096 одинаковых слов.

Соединенный и отсортированный словарь расположен по адресу /tmp/m2m_1529750790/tmp_merged_dic_sorted.txt и содержит 431394 строк.

```

## wordforms.sh
Выводит все допустимые словоформы двух словарей. Взят из [апстрима hunspell](https://github.com/hunspell/hunspell/blob/master/src/tools/wordforms).

На вход подавайте сконвертированные в UTF-8 словари. Пример конвертирования из KOI8-R в UTF-8:  
`iconv -f KOI8-R -t UTF-8 input-data/libreoffice-old/ru_RU.aff -o input-data/libreoffice-old/ru_RU.utf8.aff`  
В `input-data/` уже есть как оригиналы словарей в KOI8-R, так и они же, сконвертированные в UTF-8.

Пример запуска и вывода:
```
$ ./wordforms.sh input-data/ie-yo-aot-new/ru_RU.utf8.aff input-data/ie-yo-aot-new/ru_RU.utf8.dic яма
яму
яме
ямами
яма
ямовича
ямой
ямою
ямам
ямах
ямы
ям
```

## merge.sh
Этот скрипт должен соединить старый словарь hunspell, поддержимваемый в дереве исходников Libreoffice, с новым словарем AOT Е-Ё 0.3.9

Этот скрипт пока не готов!!

* http://forumooo.ru/index.php/topic,106.60.html
* https://extensions.openoffice.org/en/projectrelease/russkiy-orfograficheskiy-slovar-aot-eyo-russian-spellcheck-dict-ieyo-based-works
